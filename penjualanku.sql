-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Jan 2021 pada 08.36
-- Versi server: 10.4.16-MariaDB
-- Versi PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjualanku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` char(11) NOT NULL,
  `id_pemasok` char(11) DEFAULT NULL,
  `id_jenis_barang` char(50) DEFAULT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `detail_barang` text NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `id_pemasok`, `id_jenis_barang`, `nama_barang`, `detail_barang`, `harga`, `stok`) VALUES
('B0001', 'P0002', 'J0002', 'Kemeja Lengan Panjang', 'Khusus untuk pria dengan ukuran 50-100', 1500000, 13),
('B0002', 'P0001', 'J0001', 'Kemeja lengan pendek (wanita)', 'Untuk wanita dengan nomor 50-100', 100000, 120),
('B0003', 'P0002', 'J0002', 'Celana levis Panjang (pria)', 'Berwarna hitam dengan nomor 50-100', 100000, 80),
('B0004', 'P0002', 'J0002', 'Celana levis Panjang (wanita) update lagi', 'Berwarna biru dengan nomor 50-100', 120000, 50),
('B0005', 'P0003', 'J0003', 'Sepatu Nike (pria)', 'Sepatu Nike ukuran 50-100', 130000, 40),
('B0006', 'P0003', 'J0003', 'Sepatu tinggi (wanita)', 'Unruk wanita dengan ukuran 50-100', 180000, 40),
('B0007', 'P0004', 'J0004', 'Sandal monster', 'Untuk pria dan wanita ukuran 50-100', 70000, 50),
('B0008', 'P0005', 'J0005', 'Tas adidas', 'Warna hitam ', 140000, 50),
('B0009', 'P0006', 'J0006', 'Gelang tangan', 'Beraneka warna dengan kreasi berbeda', 20000, 60),
('B0084', 'P0002', 'J0002', 'Sweater', 'Sweater Biru', 250000, 90);

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id_customer` char(11) NOT NULL,
  `nama_customer` varchar(50) NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `alamat_customer` varchar(50) NOT NULL,
  `telp_customer` char(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `jenis_kelamin`, `alamat_customer`, `telp_customer`) VALUES
('C0001', 'Akbar', 'L', 'Klaten', '082222221000'),
('C0002', 'Arham Nuari', 'L', 'Yogyakarta', '082222222222'),
('C0003', 'Fitriani', 'P', 'Yogyakarta', '082222223333'),
('C0004', 'Guntur Prasetya', 'L', 'Yogyakarta', '082222224444'),
('C0005', 'Indah Murniati', 'P', 'Solo', '082222225555'),
('C0006', 'Irma Sari', 'L', 'Solo', '082222226666'),
('C0007', 'Khairunnisa', 'P', 'Klaten', '082222227777'),
('C0008', 'Leni Mardiana', 'P', 'Magelang', '082222228888'),
('C0009', 'Muh Rizal', 'L', 'Yogyakarta', '082222229999'),
('C0010', 'Rahmat Hidayat', 'L', 'Yogyakarta', '082222220000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id_barang` char(11) NOT NULL,
  `id_transaksi` char(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id_barang`, `id_transaksi`) VALUES
('B0001', 'T0001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `id_jenis_barang` char(50) NOT NULL,
  `nama_jenis_barang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_barang`
--

INSERT INTO `jenis_barang` (`id_jenis_barang`, `nama_jenis_barang`) VALUES
('J0001', 'Baju'),
('J0002', 'Celana'),
('J0003', 'Sepatu'),
('J0004', 'Sandal'),
('J0005', 'Tas'),
('J0006', 'Aksesoris'),
('J0007', 'Jaket'),
('J0008', 'Hoodie');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` char(11) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `alamat_karyawan` varchar(50) NOT NULL,
  `telp_karyawan` char(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nama_karyawan`, `jenis_kelamin`, `alamat_karyawan`, `telp_karyawan`) VALUES
('K0001', 'Andika', 'L', 'Yogyakarta', '082211112222'),
('K0002', 'Dita Leni', 'P', 'Bandung', '082211113333'),
('K0003', 'Erika Putri', 'P', 'Jakarta', '082211114444'),
('K0004', 'Gibran Raka', 'L', 'Solo', '082211115555'),
('K0005', 'Indra Pradipta', 'L', 'Kediri', '082211116666'),
('K0006', 'Irma Hidayah', 'P', 'Yogyakarta', '082211117777'),
('K0007', 'Kris Diyanto', 'L', 'Yogyakarta', '082211118888');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemasok`
--

CREATE TABLE `pemasok` (
  `id_pemasok` char(11) NOT NULL,
  `id_barang` char(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `nama_distributor` varchar(50) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `telp_perusahaan` varchar(13) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemasok`
--

INSERT INTO `pemasok` (`id_pemasok`, `id_barang`, `jumlah`, `tanggal`, `nama_distributor`, `nama_perusahaan`, `telp_perusahaan`, `alamat_perusahaan`) VALUES
('P0001', 'B0002', 100, '0000-00-00 00:00:00', 'Siswanto', 'Indo Grosir TBK', '081111111111', 'Jakarta'),
('P0002', 'B0002', 10, '0000-00-00 00:00:00', 'Budi', 'Indo Berkarya TBK', '081111112222', 'Jakarta'),
('P0003', 'B0003', 80, '2020-12-02 00:00:00', 'Utomo', 'CV Savety SNI', '081111113333', 'Jakarta'),
('P0004', 'B0004', 50, '2020-12-02 00:00:00', 'Galang', 'CV Cahaya Berkah', '081111114444', 'Jakarta'),
('P0005', 'B0005', 50, '2020-12-05 00:00:00', 'Dita', 'UD Sumber Jaya', '081111115555', 'Solo'),
('P0006', 'B0006', 60, '2020-12-05 00:00:00', 'Sentosa', 'UD Melati', '081111116666', 'Magelang'),
('P0007', 'B0007', 100, '2020-12-06 00:00:00', 'Latifa', 'PD Cahaya Utama', '081111117777', 'Surabaya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` char(11) NOT NULL,
  `id_customer` char(11) NOT NULL,
  `id_karyawan` char(11) NOT NULL,
  `jumlah_beli` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `tgl_transaksi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_customer`, `id_karyawan`, `jumlah_beli`, `total_harga`, `tgl_transaksi`) VALUES
('T0001', 'C0001', 'K0002', 2, 30000, '2020-12-25 18:26:39'),
('T0002', 'C0001', 'K0001', 10, 27000, '2020-11-09 18:26:39'),
('T0003', 'C0002', 'K0002', 45, 270000, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_jenis_barang` (`id_jenis_barang`),
  ADD KEY `id_pemasok` (`id_pemasok`);

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indeks untuk tabel `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_transaksi` (`id_transaksi`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indeks untuk tabel `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD PRIMARY KEY (`id_jenis_barang`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `pemasok`
--
ALTER TABLE `pemasok`
  ADD PRIMARY KEY (`id_pemasok`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_costomer` (`id_customer`),
  ADD KEY `id_karyawan` (`id_karyawan`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_jenis_barang`) REFERENCES `jenis_barang` (`id_jenis_barang`),
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`id_pemasok`) REFERENCES `pemasok` (`id_pemasok`);

--
-- Ketidakleluasaan untuk tabel `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`),
  ADD CONSTRAINT `detail_transaksi_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`);

--
-- Ketidakleluasaan untuk tabel `pemasok`
--
ALTER TABLE `pemasok`
  ADD CONSTRAINT `pemasok_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_karyawan`) REFERENCES `karyawan` (`id_karyawan`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
