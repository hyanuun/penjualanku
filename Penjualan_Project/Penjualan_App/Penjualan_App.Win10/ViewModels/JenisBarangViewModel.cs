﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input; // gunanya untuk agar command dikenali disini
using Penjualan_App.Win10.Models; //gunanya untuk menghubungkan Models

namespace Penjualan_App.Win10.ViewModels
{
    public class JenisBarangViewModel : BaseViewModel
    {
        Koneksi koneksi;
        JenisBarangDAO jbarang;

        public JenisBarangViewModel()
        {
            koneksi = Koneksi.GetInstance();
            jbarang = new JenisBarangDAO(koneksi.GetConnection());

            datajbarang = new ObservableCollection<JenisBarang>();
            modeljbarang = new JenisBarang();

            CreateCommand = new Command(async () => await CreateJBarangAsync());
            UpdateCommand = new Command(async () => await UpdateJBarangAsync());
            DeleteCommand = new Command(async () => await DeleteJBarangAsync());
            ReadCommand = new Command(async () => await ReadJBarangAsync(true));
            ReadCommand.Execute(null);

        }
        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<JenisBarang> DataJBarang // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return datajbarang;
            }

            set
            {
                SetProperty(ref datajbarang, value); // property yang ada di base view models
            }
        }

        public JenisBarang ModelJBarang
        {
            get
            {
                return modeljbarang;
            }

            set
            {
                SetProperty(ref modeljbarang, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<JenisBarang> datajbarang;
        private JenisBarang modeljbarang;


        private async Task InitJBarangAsync()
        {
            var data = jbarang.GetAll();

            await Task.Run(() => {
                DataJBarang = new ObservableCollection<JenisBarang>(data);
            });
        }

        private async Task ReadJBarangAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitJBarangAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<JenisBarang> ReadJBarangAsync(int id_jbarang)
        {
            await Task.Delay(0);
            return DataJBarang.Where(model => model.id_jbarang.Equals(id_jbarang)).SingleOrDefault();
        }
        private async Task CreateJBarangAsync()
        {
            jbarang.Save(ModelJBarang);
            await ReadJBarangAsync(true);
        }
        private async Task UpdateJBarangAsync()
        {
            var data = ModelJBarang;
            jbarang.Update(data);
            await ReadJBarangAsync(true);
        }

        private async Task DeleteJBarangAsync()
        {
            jbarang.Delete(ModelJBarang);
            await ReadJBarangAsync(true);
        }

    }
}
