﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input; // gunanya untuk agar command dikenali disini
using Penjualan_App.Win10.Models; //gunanya untuk menghubungkan Models


namespace Penjualan_App.Win10.ViewModels
{
    public class PemasokViewModel : BaseViewModel
    {
        Koneksi koneksi;
        PemasokDAO pemasok;
        public PemasokViewModel()
        {
            koneksi = Koneksi.GetInstance();
            pemasok = new PemasokDAO(koneksi.GetConnection());

            datapemasok = new ObservableCollection<Pemasok>();
            modelpemasok = new Pemasok();

            CreateCommand = new Command(async () => await CreatPemasokAsync());
            UpdateCommand = new Command(async () => await UpdatePemasokAsync());
            DeleteCommand = new Command(async () => await DeletePemasokAsync());
            ReadCommand = new Command(async () => await ReadPemasokAsync(true));
            ReadCommand.Execute(null);
        }

        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Pemasok> DataPemasok // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return datapemasok;
            }

            set
            {
                SetProperty(ref datapemasok, value); // property yang ada di base view models
            }
        }

        public Pemasok ModelPemasok
        {
            get
            {
                return modelpemasok;
            }

            set
            {
                SetProperty(ref modelpemasok, value);
            }
        }


        public event Action OnReload;

        private ObservableCollection<Pemasok> datapemasok;
        private Pemasok modelpemasok;

        private async Task InitPemasokAsync()
        {
            var data = pemasok.GetAll();

            await Task.Run(() => {
                DataPemasok = new ObservableCollection<Pemasok>(data);
            });
        }

        private async Task ReadPemasokAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitPemasokAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }
        private async Task<Pemasok> ReadPemasokAsync(int id_pemasok)
        {
            await Task.Delay(0);
            return DataPemasok.Where(model => model.id_pemasok.Equals(id_pemasok)).SingleOrDefault();
        }

        private async Task CreatPemasokAsync()
        {
            pemasok.Save(ModelPemasok);
            await ReadPemasokAsync(true);
        }
        private async Task UpdatePemasokAsync()
        {
            var data = ModelPemasok;
            pemasok.Update(data);
            await ReadPemasokAsync(true);
        }

        private async Task DeletePemasokAsync()
        {
            pemasok.Delete(ModelPemasok);
            await ReadPemasokAsync(true);
        }








    }
}
