﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input; // gunanya untuk agar command dikenali disini
using Penjualan_App.Win10.Models; //gunanya untuk menghubungkan Models

namespace Penjualan_App.Win10.ViewModels
{

    public class BarangViewModel : BaseViewModel //pewarisan dari base view model. harus public
    {
        Koneksi koneksi;
        BarangDao barang;

        public BarangViewModel()
        {
            koneksi = Koneksi.GetInstance();
            barang = new BarangDao(koneksi.GetConnection());

            databarang = new ObservableCollection<Barang>();
            modelbarang = new Barang();

            CreateCommand = new Command(async () => await CreateBarangAsync());
            UpdateCommand = new Command(async () => await UpdateBarangAsync());
            DeleteCommand = new Command(async () => await DeleteBarangAsync());
            ReadCommand = new Command(async () => await ReadBarangAsync(true));
            ReadCommand.Execute(null);

        }


        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Barang> DataBarang // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return databarang;
            }

            set
            {
                SetProperty(ref databarang, value); // property yang ada di base view models
            }
        }

        public Barang ModelBarang
        {
            get
            {
                return modelbarang;
            }

            set
            {
                SetProperty(ref modelbarang, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Barang> databarang;
        private Barang modelbarang;

        private async Task InitBarangAsync()
        {
            var data = barang.GetAll();

            await Task.Run(() => {
                DataBarang = new ObservableCollection<Barang>(data);
            });
        }

        private async Task ReadBarangAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitBarangAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Barang> ReadBarangAsync(int id_barang)
        {
            await Task.Delay(0);
            return DataBarang.Where(model => model.id_barang.Equals(id_barang)).SingleOrDefault();
        }

        private async Task CreateBarangAsync()
        {
            barang.Save(ModelBarang);
            await ReadBarangAsync(true);
        }

        private async Task UpdateBarangAsync()
        {
            var data = ModelBarang;
            barang.Update(data);
            await ReadBarangAsync(true);
        }

        private async Task DeleteBarangAsync()
        {
            barang.Delete(ModelBarang);
            await ReadBarangAsync(true);
        }

    }
}
