﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input; // gunanya untuk agar command dikenali disini
using Penjualan_App.Win10.Models; //gunanya untuk menghubungkan Models

namespace Penjualan_App.Win10.ViewModels
{
    public class TransaksiViewModel : BaseViewModel
    {
        Koneksi koneksi;
        TransaksiDAO transaksi;

        public TransaksiViewModel()
        {
            koneksi = Koneksi.GetInstance();
            transaksi = new TransaksiDAO(koneksi.GetConnection());

            datatransaksi = new ObservableCollection<Transaksi>();
            modeltransaksi = new Transaksi();

            CreateCommand = new Command(async () => await CreateTransaksiAsync());
            UpdateCommand = new Command(async () => await UpdateBarangAsync());
            DeleteCommand = new Command(async () => await DeleteTransaksiAsync());
            ReadCommand = new Command(async () => await ReadTransaksiAsync(true));
            ReadCommand.Execute(null);

        }
        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Transaksi> DataTransaksi // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return datatransaksi;
            }

            set
            {
                SetProperty(ref datatransaksi, value); // property yang ada di base view models
            }
        }

        public Transaksi ModelTransaksi
        {
            get
            {
                return modeltransaksi;
            }

            set
            {
                SetProperty(ref modeltransaksi, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Transaksi> datatransaksi;
        private Transaksi modeltransaksi;

        private async Task InitTransaksiAsync()
        {
            var data = transaksi.GetAll();

            await Task.Run(() => {
                DataTransaksi = new ObservableCollection<Transaksi>(data);
            });
        }

        private async Task ReadTransaksiAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitTransaksiAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Transaksi> ReadTransaksiAsync(int id_transaksi)
        {
            await Task.Delay(0);
            return DataTransaksi.Where(model => model.id_transaksi.Equals(id_transaksi)).SingleOrDefault();
        }
        private async Task CreateTransaksiAsync()
        {
            transaksi.Save(ModelTransaksi);
            await ReadTransaksiAsync(true);
        }
        private async Task UpdateBarangAsync()
        {
            var data = ModelTransaksi;
            transaksi.Update(data);
            await ReadTransaksiAsync(true);
        }
        private async Task DeleteTransaksiAsync()
        {
            transaksi.Delete(ModelTransaksi);
            await ReadTransaksiAsync(true);
        }






    }
}
