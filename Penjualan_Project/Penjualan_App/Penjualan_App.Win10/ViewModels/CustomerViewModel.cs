﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input; // gunanya untuk agar command dikenali disini
using Penjualan_App.Win10.Models; //gunanya untuk menghubungkan Models


namespace Penjualan_App.Win10.ViewModels
{
    public class CustomerViewModel : BaseViewModel
    {
        Koneksi koneksi;
        CustomerDAO customer;

        public CustomerViewModel()
        {
            koneksi = Koneksi.GetInstance();
            customer = new CustomerDAO(koneksi.GetConnection());

            datacustomer = new ObservableCollection<Customer>();
            modelcustomer = new Customer();

            CreateCommand = new Command(async () => await CreateCustomerAsync());
            UpdateCommand = new Command(async () => await UpdateCustomerAsync());
            DeleteCommand = new Command(async () => await DeleteCustomerAsync());
            ReadCommand = new Command(async () => await ReadCustomerAsync(true));
            ReadCommand.Execute(null);


        }

        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Customer> DataCustomer // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return datacustomer;
            }

            set
            {
                SetProperty(ref datacustomer, value); // property yang ada di base view models
            }
        }

        public Customer ModelCustomer
        {
            get
            {
                return modelcustomer;
            }

            set
            {
                SetProperty(ref modelcustomer, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Customer> datacustomer;
        private Customer modelcustomer;

        private async Task InitCustomerAsync()
        {
            var data = customer.GetAll();

            await Task.Run(() => {
                DataCustomer = new ObservableCollection<Customer>(data);
            });
        }
        private async Task ReadCustomerAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitCustomerAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Customer> ReadCustomerAsync(int id_customer)
        {
            await Task.Delay(0);
            return DataCustomer.Where(model => model.id_customer.Equals(id_customer)).SingleOrDefault();
        }

        private async Task CreateCustomerAsync()
        {
            customer.Save(ModelCustomer);
            await ReadCustomerAsync(true);
        }

        private async Task UpdateCustomerAsync()
        {
            var data = ModelCustomer;
            customer.Update(data);
            await ReadCustomerAsync(true);
        }

        private async Task DeleteCustomerAsync()
        {
            customer.Delete(ModelCustomer);
            await ReadCustomerAsync(true);
        }

    }
}
