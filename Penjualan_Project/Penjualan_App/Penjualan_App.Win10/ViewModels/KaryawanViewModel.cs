﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input; // gunanya untuk agar command dikenali disini
using Penjualan_App.Win10.Models; //gunanya untuk menghubungkan Models

namespace Penjualan_App.Win10.ViewModels
{
    public class KaryawanViewModel : BaseViewModel
    {
        Koneksi koneksi;
        KaryawanDAO karyawan;

        public KaryawanViewModel()
        {
            koneksi = Koneksi.GetInstance();
            karyawan = new KaryawanDAO(koneksi.GetConnection());

            datakaryawan = new ObservableCollection<Karyawan>();
            modelkaryawan = new Karyawan();

            CreateCommand = new Command(async () => await CreateKaryawanAsync());
            UpdateCommand = new Command(async () => await UpdateKaryawanAsync());
            DeleteCommand = new Command(async () => await DeleteKaryawanAsync());
            ReadCommand = new Command(async () => await ReadKaryawanAsync(true));
            ReadCommand.Execute(null);
        }

        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Karyawan> DataKaryawan // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return datakaryawan;
            }

            set
            {
                SetProperty(ref datakaryawan, value); // property yang ada di base view models
            }
        }

        public Karyawan ModelKaryawan
        {
            get
            {
                return modelkaryawan;
            }

            set
            {
                SetProperty(ref modelkaryawan, value);
            }
        }


        public event Action OnReload;

        private ObservableCollection<Karyawan> datakaryawan;
        private Karyawan modelkaryawan;

        private async Task InitKaryawanAsync()
        {
            var data = karyawan.GetAll();

            await Task.Run(() => {
                DataKaryawan = new ObservableCollection<Karyawan>(data);
            });
        }
        private async Task ReadKaryawanAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitKaryawanAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Karyawan> ReadKaryawanAsync(int id_karyawan)
        {
            await Task.Delay(0);
            return DataKaryawan.Where(model => model.id_karyawan.Equals(id_karyawan)).SingleOrDefault();
        }

        private async Task CreateKaryawanAsync()
        {
            karyawan.Save(ModelKaryawan);
            await ReadKaryawanAsync(true);
        }

        private async Task UpdateKaryawanAsync()
        {
            var data = ModelKaryawan;
            karyawan.Update(data);
            await ReadKaryawanAsync(true);
        }
        private async Task DeleteKaryawanAsync()
        {
            karyawan.Delete(ModelKaryawan);
            await ReadKaryawanAsync(true);
        }




    }
}
