﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using Penjualan_App.Win10.Views.Home;

namespace Penjualan_App.Win10
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Dashboard View { get; set; }
        public static void ViewRouting(bool flag, Control content = null)
        {
            if (flag == true)
            {
                View.PanelContent.Children.Add(content);
            } else
            {
                View.PanelContent.Children.Clear();
            }
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            View = new Dashboard();
            View.Show();
        }
    }
}
