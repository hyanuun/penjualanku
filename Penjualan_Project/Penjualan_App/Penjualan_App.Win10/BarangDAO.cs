﻿using MySql.Data.MySqlClient;
using Penjualan_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Penjualan_App.Win10
{
    class BarangDao
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;

        public BarangDao(MySqlConnection konek)
        {
            this.konek = konek;
        }

        private Barang MappingRowToObject(MySqlDataReader dtr)
        {
            string id = dtr["id_barang"] is DBNull ? string.Empty : dtr["id_barang"].ToString();

            string nama = dtr["nama_barang"] is DBNull ? string.Empty : dtr["nama_barang"].ToString();
            string deskripsi = dtr["detail_barang"] is DBNull ? string.Empty : dtr["detail_barang"].ToString();
            string id_pemasok = dtr["id_pemasok"] is DBNull ? string.Empty : dtr["id_pemasok"].ToString();
            string id_jenis_barang = dtr["id_jenis_barang"] is DBNull ? string.Empty : dtr["id_jenis_barang"].ToString();
            double harga = dtr["harga"] is DBNull ? 0 : Convert.ToDouble(dtr["harga"].ToString());
            int stok = dtr["stok"] is DBNull ? 0 : Convert.ToInt32(dtr["stok"].ToString());

            return new Barang()
            {
                id_barang = id,
                nama = nama,
                harga = harga,
                deskripsi = deskripsi,
                id_pemasok = id_pemasok,
                id_jenis_barang = id_jenis_barang,
                stok = stok
            };
        }

        public List<Barang> GetAll() //read
        {
            List<Barang> daftarbarang = new List<Barang>();

            strSql = "SELECT * FROM barang";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarbarang.Add(MappingRowToObject(dtr));
                    }
                }
            }

            return daftarbarang;
        }

        public int Save(Barang barang)
        {
            strSql = "INSERT INTO barang (id_barang, id_pemasok, id_jenis_barang, nama_barang, detail_barang, harga, stok) VALUES(@1, @2, @3, @4, @5, @6, @7)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", barang.id_barang);
                cmd.Parameters.AddWithValue("@2", barang.id_pemasok);
                cmd.Parameters.AddWithValue("@3", barang.id_jenis_barang);
                cmd.Parameters.AddWithValue("@4", barang.nama);
                cmd.Parameters.AddWithValue("@5", barang.deskripsi);
                cmd.Parameters.AddWithValue("@6", barang.harga);
                cmd.Parameters.AddWithValue("@7", barang.stok);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Barang Barang)
        {
            strSql = "DELETE FROM barang WHERE id_barang = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", Barang.id_barang);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(Barang barang)
        {
            strSql = "UPDATE barang SET id_barang = @1, nama_barang = @2 WHERE id_barang = @3";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", barang.id_barang);
                cmd.Parameters.AddWithValue("@2", barang.nama);
                cmd.Parameters.AddWithValue("@3", barang.id_barang);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
