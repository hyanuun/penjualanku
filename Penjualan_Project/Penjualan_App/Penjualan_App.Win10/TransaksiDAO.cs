﻿using MySql.Data.MySqlClient;
using Penjualan_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Penjualan_App.Win10
{
    class TransaksiDAO
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;
        public TransaksiDAO(MySqlConnection konek)
        {
            this.konek = konek;
        }
        private Transaksi MappingRowToObject(MySqlDataReader dtr)
        {
            string id_transaksi = dtr["id_transaksi"] is DBNull ? string.Empty : dtr["id_transaksi"].ToString();
            string id_customer = dtr["id_customer"] is DBNull ? string.Empty : dtr["id_customer"].ToString();
            string id_karyawan = dtr["id_karyawan"] is DBNull ? string.Empty : dtr["id_karyawan"].ToString();
            int jumlah = dtr["jumlah_beli"] is DBNull ? 0 : Convert.ToInt32(dtr["jumlah_beli"].ToString());
            double totalharga = dtr["total_harga"] is DBNull ? 0 : Convert.ToDouble(dtr["total_harga"].ToString());
            string tanggal = dtr["tgl_transaksi"] is DBNull ? string.Empty : dtr["tgl_transaksi"].ToString();


            return new Transaksi()
            {
                id_transaksi = id_transaksi,
                id_customer = id_customer,
                id_karyawan = id_karyawan,
                jumlah = jumlah,
                totalharga = totalharga,
                tanggal = tanggal
            };
        }
        public List<Transaksi> GetAll() //read
        {
            List<Transaksi> daftartransaksi = new List<Transaksi>();

            strSql = "SELECT * FROM transaksi";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftartransaksi.Add(MappingRowToObject(dtr));
                    }
                }
            }

            return daftartransaksi;
        }

        public int Save(Transaksi transaksi)
        {
            strSql = "INSERT INTO transaksi (id_transaksi, id_customer, id_karyawan, jumlah_beli, total_harga, tgl_transaksi) VALUES(@1, @2, @3, @4, @5, @6)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", transaksi.id_transaksi);
                cmd.Parameters.AddWithValue("@2", transaksi.id_customer);
                cmd.Parameters.AddWithValue("@3", transaksi.id_karyawan);
                cmd.Parameters.AddWithValue("@4", transaksi.jumlah);
                cmd.Parameters.AddWithValue("@5", transaksi.totalharga);
                cmd.Parameters.AddWithValue("@6", transaksi.tanggal);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Transaksi transaksi)
        {
            strSql = "DELETE FROM transaksi WHERE id_transaksi = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", transaksi.id_transaksi);
                return cmd.ExecuteNonQuery();
            }
        }
        public int Update(Transaksi transaksi)
        {
            strSql = "UPDATE transaksi SET id_transaksi = @1, id_customer = @2, id_karyawan = @3, jumlah_beli = @4, total_harga = @5, tgl_transaksi = @6 WHERE id_transaksi = @7";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", transaksi.id_transaksi);
                cmd.Parameters.AddWithValue("@2", transaksi.id_customer);
                cmd.Parameters.AddWithValue("@3", transaksi.id_karyawan);
                cmd.Parameters.AddWithValue("@4", transaksi.jumlah);
                cmd.Parameters.AddWithValue("@5", transaksi.totalharga);
                cmd.Parameters.AddWithValue("@6", transaksi.tanggal);
                cmd.Parameters.AddWithValue("@7", transaksi.id_transaksi);
                return cmd.ExecuteNonQuery();
            }
        }

        




    }
}
