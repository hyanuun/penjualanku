﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Penjualan_App.Win10.Views.Home
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            InitializeComponent();
        }


        private void ButtonCustomer_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new User.CustomerView());
        }



        private void ButtonKaryawan_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new User.KaryawanView());
        }

        private void ButtonPemasok_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new MasterData.PemasokView());
        }

        private void ButtonTransaksi_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new MasterData.TransaksiView());
        }

        private void ButtonBarang_Click(object sender, RoutedEventArgs e)
        {
            //new MasterData.BarangForm().ShowDialog();
            App.ViewRouting(false);
            App.ViewRouting(true, new MasterData.BarangView());
        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void MenuBarang_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new MasterData.BarangView());
        }

        private void MenuPemasok_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new MasterData.PemasokView());
        }

        private void MenuCustomer_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new User.CustomerView());
        }

        private void MenuKaryawan_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new User.KaryawanView());
        }

        private void MenuJenisBarang_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new MasterData.JenisBarangView());
        }

        private void ButtonJenisBarang_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new MasterData.JenisBarangView());
        }

        private void MenuTransaksi_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new MasterData.TransaksiView());
        }
    }
}
