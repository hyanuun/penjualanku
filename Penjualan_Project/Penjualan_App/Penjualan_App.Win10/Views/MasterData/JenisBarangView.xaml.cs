﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading.Tasks;
using Penjualan_App.Win10.ViewModels;

namespace Penjualan_App.Win10.Views.MasterData
{
    /// <summary>
    /// Interaction logic for JenisBarangView.xaml
    /// </summary>
    public partial class JenisBarangView : UserControl
    {
        public JenisBarangView()
        {
            InitializeComponent();
            vm = new JenisBarangViewModel();
            vm.OnReload += () =>
            {
                ListData.ItemsSource = null;
                ListData.ItemsSource = vm.DataJBarang;
                if (form != null)
                {
                    form.Close();
                }
                vm.ModelJBarang = null;
                ButtonEdit.Visibility = Visibility.Hidden;
                ButtonReset.Visibility = Visibility.Hidden;
            };
            ButtonEdit.Visibility = Visibility.Hidden;
            ButtonReset.Visibility = Visibility.Hidden;
            DataContext = vm;
        }

        private JenisBarangViewModel vm;
        private JenisBarangForm form;
        private async Task InitFormAsync()
        {
            await Task.Delay(0);
            form = new JenisBarangForm(vm);
            form.ShowDialog();
        }

        private async void ListData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await Task.Delay(0);
            if (vm.ModelJBarang != null)
            {
                ButtonEdit.Visibility = Visibility.Visible;
                ButtonReset.Visibility = Visibility.Visible;
            }
        }
        private async void ButtonNew_Click(object sender, RoutedEventArgs e)
        {
            vm.ModelJBarang = null;
            await InitFormAsync();
        }

        private async void ButtonEdit_Click(object sender, RoutedEventArgs e)
        {
            await InitFormAsync();
        }

        private async void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            await Task.Delay(0);
            vm.ModelJBarang = null;
            ButtonEdit.Visibility = Visibility.Hidden;
            ButtonReset.Visibility = Visibility.Hidden;

        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
        }
    }
}
