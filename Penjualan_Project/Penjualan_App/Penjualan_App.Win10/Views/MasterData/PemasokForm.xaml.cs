﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using Penjualan_App.Win10.ViewModels;
using Penjualan_App.Win10.Models;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Penjualan_App.Win10.Views.MasterData
{
    /// <summary>
    /// Interaction logic for PemasokForm.xaml
    /// </summary>
    public partial class PemasokForm : Window
    {
        public PemasokForm(PemasokViewModel vm)
        {
            InitializeComponent();
            if (vm.ModelPemasok == null)
            {
                vm.ModelPemasok = new Pemasok();
                ButtonDelete.Visibility = Visibility.Hidden;
                ButtonUpdate.Visibility = Visibility.Hidden;
                ButtonSave.Visibility = Visibility.Visible;
            }
            else
            {
                ButtonDelete.Visibility = Visibility.Visible;
                ButtonUpdate.Visibility = Visibility.Visible;
                ButtonSave.Visibility = Visibility.Hidden;
            }
            DataContext = vm;
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
