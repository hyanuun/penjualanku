﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading.Tasks;
using Penjualan_App.Win10.ViewModels;

namespace Penjualan_App.Win10.Views.User
{
    /// <summary>
    /// Interaction logic for CustomerView.xaml
    /// </summary>
    public partial class CustomerView : UserControl
    {
        public CustomerView()
        {
            InitializeComponent();
            vm = new CustomerViewModel();
            vm.OnReload += () =>
            {
                ListData.ItemsSource = null;
                ListData.ItemsSource = vm.DataCustomer;
                if (form != null)
                {
                    form.Close();
                }
                vm.ModelCustomer = null;
                ButtonEdit.Visibility = Visibility.Hidden;
                ButtonReset.Visibility = Visibility.Hidden;
            };
            ButtonEdit.Visibility = Visibility.Hidden;
            ButtonReset.Visibility = Visibility.Hidden;
            DataContext = vm;
        }

        private CustomerViewModel vm;
        private CustomerForm form;

        private async Task InitFormAsync()
        {
            await Task.Delay(0);
            form = new CustomerForm(vm);
            form.ShowDialog();
        }

        private async void ListData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await Task.Delay(0);
            if (vm.ModelCustomer != null)
            {
                ButtonEdit.Visibility = Visibility.Visible;
                ButtonReset.Visibility = Visibility.Visible;
            }
        }

        private async void ButtonNew_Click(object sender, RoutedEventArgs e)
        {
            vm.ModelCustomer = null;
            await InitFormAsync();
        }

        private async void ButtonEdit_Click(object sender, RoutedEventArgs e)
        {
            await InitFormAsync();
        }

        private async void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            await Task.Delay(0);
            vm.ModelCustomer = null;
            ButtonEdit.Visibility = Visibility.Hidden;
            ButtonReset.Visibility = Visibility.Hidden;

        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
        }
    }
}
