﻿using MySql.Data.MySqlClient;
using Penjualan_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Penjualan_App.Win10
{
    class JenisBarangDAO
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;

        public JenisBarangDAO(MySqlConnection konek)
        {
            this.konek = konek;
        }

        private JenisBarang MappingRowToObject(MySqlDataReader dtr)
        {
            string id_jbarang = dtr["id_jenis_barang"] is DBNull ? string.Empty : dtr["id_jenis_barang"].ToString();

            string nama_jbarang = dtr["nama_jenis_barang"] is DBNull ? string.Empty : dtr["nama_jenis_barang"].ToString();

            return new JenisBarang()
            {
                id_jbarang = id_jbarang,
                nama_jbarang = nama_jbarang
            };
        }
        public List<JenisBarang> GetAll() //read
        {
            List<JenisBarang> daftarjbarang = new List<JenisBarang>();

            strSql = "SELECT * FROM jenis_barang";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarjbarang.Add(MappingRowToObject(dtr));
                    }
                }
            }

            return daftarjbarang;
        }
        public int Save(JenisBarang jbarang)
        {
            strSql = "INSERT INTO jenis_barang (id_jenis_barang, nama_jenis_barang) VALUES(@1, @2)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", jbarang.id_jbarang);
                cmd.Parameters.AddWithValue("@2", jbarang.nama_jbarang);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(JenisBarang jbarang)
        {
            strSql = "DELETE FROM jenis_barang WHERE id_jenis_barang = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", jbarang.id_jbarang);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(JenisBarang jbarang)
        {
            strSql = "UPDATE jenis_barang SET id_jenis_barang = @1, nama_jenis_barang = @2 WHERE id_jenis_barang = @3";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", jbarang.id_jbarang);
                cmd.Parameters.AddWithValue("@2", jbarang.nama_jbarang);
                cmd.Parameters.AddWithValue("@3", jbarang.id_jbarang);
                return cmd.ExecuteNonQuery();
            }
        }










    }
}
