﻿using MySql.Data.MySqlClient;
using Penjualan_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Penjualan_App.Win10
{
    class KaryawanDAO
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;
        public KaryawanDAO(MySqlConnection konek)
        {
            this.konek = konek;
        }
        private Karyawan MappingRowToObject(MySqlDataReader dtr)
        {
            string id_karyawan = dtr["id_karyawan"] is DBNull ? string.Empty : dtr["id_karyawan"].ToString();

            string nama = dtr["nama_karyawan"] is DBNull ? string.Empty : dtr["nama_karyawan"].ToString();
            string jk = dtr["jenis_kelamin"] is DBNull ? string.Empty : dtr["jenis_kelamin"].ToString();
            string alamat = dtr["alamat_karyawan"] is DBNull ? string.Empty : dtr["alamat_karyawan"].ToString();
            string telp = dtr["telp_karyawan"] is DBNull ? string.Empty : dtr["telp_karyawan"].ToString();

            return new Karyawan()
            {
                id_karyawan = id_karyawan,
                nama = nama,
                jk = jk,
                alamat = alamat,
                telp = telp
            };
        }
        public List<Karyawan> GetAll() //read
        {
            List<Karyawan> daftarkaryawan = new List<Karyawan>();

            strSql = "SELECT * FROM karyawan";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarkaryawan.Add(MappingRowToObject(dtr));
                    }
                }
            }

            return daftarkaryawan;
        }

        public int Save(Karyawan karyawan)
        {
            strSql = "INSERT INTO karyawan (id_karyawan, nama_karyawan, jenis_kelamin, alamat_karyawan, telp_karyawan) VALUES(@1, @2, @3, @4, @5)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", karyawan.id_karyawan);
                cmd.Parameters.AddWithValue("@2", karyawan.nama);
                cmd.Parameters.AddWithValue("@3", karyawan.jk);
                cmd.Parameters.AddWithValue("@4", karyawan.alamat);
                cmd.Parameters.AddWithValue("@5", karyawan.telp);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Karyawan karyawan)
        {
            strSql = "DELETE FROM karyawan WHERE id_karyawan = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", karyawan.id_karyawan);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(Karyawan karyawan)
        {
            strSql = "UPDATE karyawan SET id_karyawan = @1, nama_karyawan = @2, jenis_kelamin = @3, alamat_karyawan = @4, telp_karyawan = @5 WHERE id_karyawan = @6";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", karyawan.id_karyawan);
                cmd.Parameters.AddWithValue("@2", karyawan.nama);
                cmd.Parameters.AddWithValue("@3", karyawan.jk);
                cmd.Parameters.AddWithValue("@4", karyawan.alamat);
                cmd.Parameters.AddWithValue("@5", karyawan.telp);
                cmd.Parameters.AddWithValue("@6", karyawan.id_karyawan);
                return cmd.ExecuteNonQuery();
            }
        }












    }
}
