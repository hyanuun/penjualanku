﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Penjualan_App.Win10
{
    class Koneksi
    {
        private MySqlConnection konek = null;
        private static Koneksi dbkoneksi = null;

        private Koneksi()
        {
            if (konek == null)
            {
                string server = "localhost";
                string database = "penjualanku";
                string user = "root";
                string password = "";
                string ConvertZeroDateTime = "true";
                string strkoneksi = "SERVER=" + server + ";DATABASE=" + database + ";UID=" + user + ";PASSWORD=" + password + ";Convert Zero DateTime =" + ConvertZeroDateTime;
                konek = new MySqlConnection(strkoneksi);
                konek.Open();

            }
        }

        public static Koneksi GetInstance()
        {
            if (dbkoneksi == null)
            {
                dbkoneksi = new Koneksi();
            }

            return dbkoneksi;
        }

        public MySqlConnection GetConnection()
        {
            return this.konek;
        }
    }
}
