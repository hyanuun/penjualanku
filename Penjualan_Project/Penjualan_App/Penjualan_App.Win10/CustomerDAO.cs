﻿using MySql.Data.MySqlClient;
using Penjualan_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Penjualan_App.Win10
{
    class CustomerDAO
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;
        public CustomerDAO(MySqlConnection konek)
        {
            this.konek = konek;
        }
        private Customer MappingRowToObject(MySqlDataReader dtr)
        {
            string id_customer = dtr["id_customer"] is DBNull ? string.Empty : dtr["id_customer"].ToString();

            string nama = dtr["nama_customer"] is DBNull ? string.Empty : dtr["nama_customer"].ToString();
            string jk = dtr["jenis_kelamin"] is DBNull ? string.Empty : dtr["jenis_kelamin"].ToString();
            string alamat = dtr["alamat_customer"] is DBNull ? string.Empty : dtr["alamat_customer"].ToString();
            string telp = dtr["telp_customer"] is DBNull ? string.Empty : dtr["telp_customer"].ToString();
            
            return new Customer()
            {
                id_customer = id_customer,
                nama = nama,
                jk = jk,
                alamat = alamat,
                telp = telp
            };
        }
        public List<Customer> GetAll() //read
        {
            List<Customer> daftarcustomer = new List<Customer>();

            strSql = "SELECT * FROM customer";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarcustomer.Add(MappingRowToObject(dtr));
                    }
                }
            }

            return daftarcustomer;
        }

        public int Save(Customer customer)
        {
            strSql = "INSERT INTO customer (id_customer, nama_customer, jenis_kelamin, alamat_customer, telp_customer) VALUES(@1, @2, @3, @4, @5)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", customer.id_customer);
                cmd.Parameters.AddWithValue("@2", customer.nama);
                cmd.Parameters.AddWithValue("@3", customer.jk);
                cmd.Parameters.AddWithValue("@4", customer.alamat);
                cmd.Parameters.AddWithValue("@5", customer.telp);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Customer customer)
        {
            strSql = "DELETE FROM customer WHERE id_customer = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", customer.id_customer);
                return cmd.ExecuteNonQuery();
            }
        }
        public int Update(Customer customer)
        {
            strSql = "UPDATE customer SET id_customer = @1, nama_customer = @2, jenis_kelamin = @3, alamat_customer = @4, telp_customer = @5 WHERE id_customer = @6";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", customer.id_customer);
                cmd.Parameters.AddWithValue("@2", customer.nama);
                cmd.Parameters.AddWithValue("@3", customer.jk);
                cmd.Parameters.AddWithValue("@4", customer.alamat);
                cmd.Parameters.AddWithValue("@5", customer.telp);
                cmd.Parameters.AddWithValue("@6", customer.id_customer);
                return cmd.ExecuteNonQuery();
            }
        }














    }
}
