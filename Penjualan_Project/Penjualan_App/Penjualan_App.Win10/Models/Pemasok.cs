﻿
namespace Penjualan_App.Win10.Models
{
    public class Pemasok
    {
        public string id_pemasok { get; set; }
        public string id_barang { get; set; }
        public int jumlah { get; set; }
        public string tanggal { get; set; }
        public string nama_distributor { get; set; }
        public string nama_perusahaan { get; set; }
        public string telp { get; set; }
        public string alamat { get; set; }
    }
}
