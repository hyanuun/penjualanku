﻿namespace Penjualan_App.Win10.Models
{
    public class Barang
    {
        public string id_barang { get; set; }
        public string id_pemasok { get; set; }
        public string id_jenis_barang { get; set; }
        public string nama { get; set; }

        public double harga { get; set; }
        public int stok { get; set; }
        public string deskripsi { get; set; }


    }
}
