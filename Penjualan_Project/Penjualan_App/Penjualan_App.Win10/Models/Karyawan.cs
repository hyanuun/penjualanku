﻿namespace Penjualan_App.Win10.Models
{
    public class Karyawan
    {
        public string id_karyawan { get; set; }
        public string nama { get; set; }
        public string jk { get; set; }
        public string alamat { get; set; }
        public string telp { get; set; }
    }
}
