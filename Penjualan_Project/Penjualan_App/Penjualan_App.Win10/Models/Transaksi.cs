﻿namespace Penjualan_App.Win10.Models
{
    public class Transaksi
    {
        public string id_transaksi { get; set; }
        public string id_customer { get; set; }
        public string id_karyawan { get; set; }
        public int jumlah { get; set; }
        public double totalharga { get; set; }
        public string tanggal { get; set; }
        
        //public 
    }
}
