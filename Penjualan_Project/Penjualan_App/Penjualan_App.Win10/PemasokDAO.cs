﻿using MySql.Data.MySqlClient;
using Penjualan_App.Win10.Models;
using System;
using System.Collections.Generic;


namespace Penjualan_App.Win10
{
    class PemasokDAO
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;

        public PemasokDAO(MySqlConnection konek)
        {
            this.konek = konek;
        }
        private Pemasok MappingRowToObject(MySqlDataReader dtr)
        {
            string id_pemasok = dtr["id_pemasok"] is DBNull ? string.Empty : dtr["id_pemasok"].ToString();
            string id_barang = dtr["id_barang"] is DBNull ? string.Empty : dtr["id_barang"].ToString();
            int jumlah = dtr["jumlah"] is DBNull ? 0 : Convert.ToInt32(dtr["jumlah"].ToString());
            string tanggal = dtr["tanggal"] is DBNull ? string.Empty : dtr["tanggal"].ToString();
            string nama_distributor = dtr["nama_distributor"] is DBNull ? string.Empty : dtr["nama_distributor"].ToString();
            string nama_perusahaan = dtr["nama_perusahaan"] is DBNull ? string.Empty : dtr["nama_perusahaan"].ToString();
            string telp = dtr["telp_perusahaan"] is DBNull ? string.Empty : dtr["telp_perusahaan"].ToString();
            string alamat = dtr["alamat_perusahaan"] is DBNull ? string.Empty : dtr["alamat_perusahaan"].ToString();


            return new Pemasok()
            {
                id_pemasok = id_pemasok,
                id_barang = id_barang,
                jumlah = jumlah,
                tanggal = tanggal,
                nama_distributor = nama_distributor,
                nama_perusahaan = nama_perusahaan,
                telp = telp,
                alamat = alamat
            };
        }

        public List<Pemasok> GetAll() //read
        {
            List<Pemasok> daftarpemasok = new List<Pemasok>();

            strSql = "SELECT * FROM pemasok";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarpemasok.Add(MappingRowToObject(dtr));
                    }
                }
            }

            return daftarpemasok;
        }
        public int Save(Pemasok pemasok)
        {
            strSql = "INSERT INTO pemasok (id_pemasok, id_barang, jumlah, tanggal, nama_distributor, nama_perusahaan, telp_perusahaan, alamat_perusahaan) VALUES(@1, @2, @3, @4, @5, @6, @7, @8)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {

                cmd.Parameters.AddWithValue("@1", pemasok.id_pemasok);
                cmd.Parameters.AddWithValue("@2", pemasok.id_barang);
                cmd.Parameters.AddWithValue("@3", pemasok.jumlah);
                cmd.Parameters.AddWithValue("@4", pemasok.tanggal);
                cmd.Parameters.AddWithValue("@5", pemasok.nama_distributor);
                cmd.Parameters.AddWithValue("@6", pemasok.nama_perusahaan);
                cmd.Parameters.AddWithValue("@7", pemasok.telp);
                cmd.Parameters.AddWithValue("@8", pemasok.alamat);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Pemasok pemasok)
        {
            strSql = "DELETE FROM pemasok WHERE id_pemasok = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", pemasok.id_pemasok);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(Pemasok pemasok)
        {
            strSql = "UPDATE pemasok SET id_pemasok = @1, id_barang = @2, jumlah = @3, tanggal = @4, nama_distributor = @5, nama_perusahaan = @6, telp_perusahaan = @7, alamat_perusahaan =@8 WHERE id_pemasok = @9";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", pemasok.id_pemasok);
                cmd.Parameters.AddWithValue("@2", pemasok.id_barang);
                cmd.Parameters.AddWithValue("@3", pemasok.jumlah);
                cmd.Parameters.AddWithValue("@4", pemasok.tanggal);
                cmd.Parameters.AddWithValue("@5", pemasok.nama_distributor);
                cmd.Parameters.AddWithValue("@6", pemasok.nama_perusahaan);
                cmd.Parameters.AddWithValue("@7", pemasok.telp);
                cmd.Parameters.AddWithValue("@8", pemasok.alamat);

                cmd.Parameters.AddWithValue("@9", pemasok.id_pemasok);
                return cmd.ExecuteNonQuery();
            }
        }
















    }
}
